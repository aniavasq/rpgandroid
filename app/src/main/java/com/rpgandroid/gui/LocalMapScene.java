package com.rpgandroid.gui;

import android.os.Bundle;

import com.badlogic.gdx.physics.box2d.Body;
import com.rpgandroid.manager.SceneManager.SceneType;

import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.extension.tmx.TMXTileProperty;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.debug.Debug;

import java.io.IOException;

/**
 * @author anibal
 * @since 27/03/16.
 */
public class LocalMapScene extends BaseScene {
    private static final String TILE_MAP = "b_w_outdoor.tmx";

    private String monsterID;

    private ITexture mPlayerTexture;
    private ITexture grassTexture;
    private TiledTextureRegion mPlayerTextureRegion;
    private TextureRegion grassTextureRegion;
    private Body mPlayerBody;

    private ITexture mOnScreenControlBaseTexture;
    private ITextureRegion mOnScreenControlBaseTextureRegion;
    private ITexture mOnScreenControlKnobTexture;
    private ITextureRegion mOnScreenControlKnobTextureRegion;

    private TMXTiledMap mTMXTiledMap;
    private TMXLayer[] waterLayers = new TMXLayer[5];

    private void loadGameGraphics() throws IOException {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        /*Bundle b = getIntent().getExtras();
        String value = b.getString("monster");*/

        this.mPlayerTexture = new AssetBitmapTexture(activity.getTextureManager(),
                activity.getAssets(), "gfx/monster/" + monsterID + ".png", TextureOptions.DEFAULT);
        this.mPlayerTextureRegion =
                TextureRegionFactory.extractTiledFromTexture(this.mPlayerTexture, 4, 4);
        this.mPlayerTexture.load();

        this.grassTexture = new AssetBitmapTexture(activity.getTextureManager(),
                activity.getAssets(), "gfx/grass.png", TextureOptions.DEFAULT);
        this.grassTextureRegion = TextureRegionFactory.extractFromTexture(this.grassTexture);
        this.grassTexture.load();

        // Control texture
        this.mOnScreenControlBaseTexture = new AssetBitmapTexture(this.activity.getTextureManager(),
                this.activity.getAssets(), "gfx/onscreen_control_base.png", TextureOptions.BILINEAR);
        this.mOnScreenControlBaseTextureRegion =
                TextureRegionFactory.extractFromTexture(this.mOnScreenControlBaseTexture);
        this.mOnScreenControlBaseTexture.load();

        this.mOnScreenControlKnobTexture = new AssetBitmapTexture(this.activity.getTextureManager(),
                this.activity.getAssets(), "gfx/onscreen_control_knob.png", TextureOptions.BILINEAR);
        this.mOnScreenControlKnobTextureRegion =
                TextureRegionFactory.extractFromTexture(this.mOnScreenControlKnobTexture);
        this.mOnScreenControlKnobTexture.load();

        try {
            final TMXLoader tmxLoader = new TMXLoader(this.activity.getAssets(),
                    this.activity.getTextureManager(), TextureOptions.NEAREST_PREMULTIPLYALPHA,
                    this.vbom, getTilePropertiesListener());
            this.mTMXTiledMap = tmxLoader.loadFromAsset(TILE_MAP);

            this.mTMXTiledMap.setOffsetCenter(0, 0);

            //this.toastOnUiThread("Cactus count in this TMXTiledMap: " + TileActivity.this.mCactusCount, Toast.LENGTH_LONG);
        } catch (final TMXLoadException e) {
            Debug.e(e);
        }
    }

    private TMXLoader.ITMXTilePropertiesListener getTilePropertiesListener() {
        return new TMXLoader.ITMXTilePropertiesListener() {

            @Override
            public void onTMXTileWithPropertiesCreated(final TMXTiledMap pTMXTiledMap,
                                                       final TMXLayer pTMXLayer,
                                                       final TMXTile pTMXTile,
                                                       final TMXProperties<TMXTileProperty> pTMXTileProperties) {
					/* We are going to count the tiles that have the property "cactus=true" set. */
                //if(pTMXTileProperties.containsTMXProperty("grass", "true")) {
                //TileActivity.this.mCactusCount++;final
                //}
            }
        };
    }

    @Override
    public void createScene() {

    }

    @Override
    public void onBackKeyPressed() {

    }

    @Override
    public SceneType getSceneType() {
        return null;
    }

    @Override
    public void disposeScene() {

    }
}
