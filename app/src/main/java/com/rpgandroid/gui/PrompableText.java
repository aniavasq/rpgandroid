package com.rpgandroid.gui;

/**
 * @author anibal
 * @since 05/10/16.
 */
public class PrompableText {
    String text;
    Integer linesNumber;
    String[] lines;
    Boolean scrollable;

    public PrompableText(){  }

    public PrompableText(String text) {
        this.text = text;
        this.lines = text.split("\n");
        this.linesNumber = this.lines.length;
        this.scrollable = this.linesNumber > 3;
    }

    public String getText() {
        return this.text;
    }

    public Integer getLinesNumber() {
        return this.linesNumber;
    }

    public String[] getLines() {
        return this.lines;
    }

    public Boolean isScrollable() {
        return this.scrollable;
    }
}
