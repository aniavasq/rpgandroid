package com.rpgandroid.logic;

import com.rpgandroid.logic.action.AIDecide;
import com.rpgandroid.logic.action.Action;
import com.rpgandroid.logic.action.IAction;
import com.rpgandroid.logic.action.PlayerDecide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 26/03/16.
 */
public class BattleState implements IState {
    Stack<Action> mActions;
    List<Entity> mEntities;

    StateMachine mBattleStates = new StateMachine();

    /* public static boolean sortByTime(Action a, Action b) {
        return a.TimeRemaining() > b.TimeRemaining()
    } */

    public BattleState() {
        mBattleStates.Add("tick", new BattleTick(mBattleStates, mActions));
        mBattleStates.Add("execute", new BattleExecute(mBattleStates, mActions));
    }

    public void OnEnter(Object... params) {
        mBattleStates.Change("tick");

        //
        // Get a decision action for every entity in the action queue
        // The sort it so the quickest actions are the top
        //

        mEntities = (ArrayList<Entity>) params[0];

        for(Entity e : mEntities) {
            if(e.isPlayerControlled()) {
                PlayerDecide action = new PlayerDecide(e, e.getSpeed());
                mActions.add(action);
            } else {
                AIDecide action = new AIDecide(e, e.getSpeed());
                mActions.add(action);
            }
        }

        // Sort(mActions, BattleState.sortByTime);
        Arrays.sort(mActions.toArray(), Action.ActionComparator);
    }

    public void Update(float elapsedTime, Object... params) throws ExecutionException, InterruptedException {
        mBattleStates.Update(elapsedTime, params);
    }

    public void Render() {
        // Draw the scene, gui, characters, animations etc

        mBattleStates.Render();
    }

    public void OnExit() { }
}
