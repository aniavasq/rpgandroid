package com.rpgandroid.logic;

import java.lang.reflect.Method;

/**
 * @author anibal
 * @since 09/10/16.
 */

public interface IEventListener<T> {
    T onEvent(Method method, Object...params);
}
