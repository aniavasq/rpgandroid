package com.rpgandroid.logic;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.rpgandroid.manager.datamanager.EncounterMethod;
import com.rpgandroid.manager.datamanager.GenderRatio;
import com.rpgandroid.manager.datamanager.Monster;
import com.rpgandroid.manager.datamanager.MonsterBuilder;
import com.rpgandroid.manager.datamanager.Zone;
import com.rpgandroid.rpgandroid.DataBaseHelper;
import com.rpgandroid.rpgandroid.R;
import com.rpgandroid.rpgandroid.TileActivity;
import com.rpgandroid.rpgandroid.Utils;

import org.andengine.entity.text.Text;
import org.andengine.util.debug.Debug;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

/**
 * @author anibal
 * @since 08/10/16.
 */
public class Battle {
    private Monster monster;
    private View battleView;
    // private TextView label1;
    // private TextView label2;
    private Activity activity;
    // private boolean isShowingInfo = false;
    private Zone zone;
    private EncounterMethod encounterMethod;

    private ViewGroup alliesMonsterInfoView;
    private ViewGroup foesMonsterInfoView;

    private ViewGroup battleMenuViewGroup;

    public Battle(View battleView,
                  Activity activity,
                  Monster monster,
                  Zone zone,
                  EncounterMethod encounterMethod
    ) {
        this.battleView = battleView;
        this.activity = activity;
        this.monster = monster;
        this.zone = zone;
        this.encounterMethod = encounterMethod;

        this.battleView.setVisibility(View.GONE);

        this.alliesMonsterInfoView = (ViewGroup) this.battleView.findViewById(R.id.battle_monster_allies_info);
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Battle.this.alliesMonsterInfoView.removeAllViews();
            }
        });
        this.battleInfoViewFromMonster(this.monster, this.alliesMonsterInfoView);

        if (zone != null) {
            this.foesMonsterInfoView = (ViewGroup) this.battleView.findViewById(R.id.battle_monster_foes_info);
            this.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Battle.this.foesMonsterInfoView.removeAllViews();
                }
            });
            Monster foeMonster = this.getRandomMonster();
            Debug.e((foeMonster==null) + "");
            if (foeMonster != null) {
                this.battleInfoViewFromMonster(foeMonster, this.foesMonsterInfoView);
                ViewGroup battleMenuViewGroup = (ViewGroup) this.battleView.findViewById(R.id.battle_menu);
                String info = String.format(Locale.getDefault(), this.activity.getString(R.string.menu_label_1), foeMonster.getName());
                this.showBattleInfo(info, battleMenuViewGroup);
            }
        }

        /*
        this.battleView.setVisibility(View.GONE);
        Button runButton = (Button) this.battleView.findViewById(R.id.battle_action_1);
        Button pkmButton = (Button) this.battleView.findViewById(R.id.battle_action_2);
        label1 = (TextView) this.battleView.findViewById(R.id.battle_text_label);
        label2 = (TextView) this.battleView.findViewById(R.id.battle_label_2);

        runButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitBattle();
            }
        });
        pkmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monsterInfo();
            }
        });
        */
    }

    public void recursiveLoopChildren(ViewGroup parent) {
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            final View child = parent.getChildAt(i);
            if (child instanceof ViewGroup) {
                recursiveLoopChildren((ViewGroup) child);
                // DO SOMETHING WITH VIEWGROUP, AFTER CHILDREN HAS BEEN LOOPED
            } else {
                if (child != null) {
                    // DO SOMETHING WITH VIEW
                }
            }
        }
    }

    public void showBattle() {
        this.battleView.setVisibility(View.VISIBLE);
    }

    public void hideBattle() {
        this.battleView.setVisibility(View.GONE);
    }

    private Monster getRandomMonster() {
        try {
            DataBaseHelper dbh = new DataBaseHelper(this.activity.getBaseContext());
            SQLiteDatabase db = dbh.getWritableDatabase();
            String queryString = "SELECT *" +
                    "FROM encounters a " +
                    "LEFT JOIN encounter_slots b " +
                    "ON a.encounter_slot_id=b.id " +
                    "WHERE a.location_area_id=? AND version_id=10 AND encounter_method_id=? " +
                    "ORDER BY pokemon_id;";
            String[] queryArgs = {this.zone.getId(), this.encounterMethod.getEncounterMethodId()};
            ArrayList<String> encounterSlots = new ArrayList<>();
            encounterSlots.add("");
            Cursor slotCursor = db.rawQuery(queryString, queryArgs);
            while(slotCursor.moveToNext()) {
                encounterSlots.add(slotCursor.getString(4));
            }
            slotCursor.close();

            queryString = "SELECT * " +
                    "FROM pokemon a " +
                    "INNER JOIN pokemon_forms b ON a.id=b. pokemon_id " +
                    "LEFT JOIN  pokemon_species c ON a.id = c.id " +
                    "WHERE a.id=?";
            queryArgs = new String[]{encounterSlots.get(Utils.getValueInRange(0, encounterSlots.size() - 1))};
            Cursor monsterCursor = db.rawQuery(queryString, queryArgs);
            Monster foeMonster = null;
            while(monsterCursor.moveToNext()) {
                MonsterBuilder monsterBuilder = new MonsterBuilder()
                        .setNumber(monsterCursor.getInt(0))
                        .setName(monsterCursor.getString(9))
                        .setForm(monsterCursor.getInt(16))
                        .setLevel(50)
                        .setGenderRatio(new GenderRatio(monsterCursor.getInt(26)))
                        .setEffortValues(new int[]{0, 0, 0, 0, 0, 0});
                foeMonster = monsterBuilder.createMonster();
            }
            if (foeMonster != null) {
                foeMonster.generateId();
                foeMonster.generateRandomAttribute(dbh);
                // label1.setText(this.activity.getString(R.string.menu_label_1, foeMonster.getName()));
                return foeMonster;
            }
            monsterCursor.close();
        } catch (IOException e) {
            Debug.e(e.getMessage(), e);
        }
        return null;
    }

    public void exitBattle() {
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Battle.this.battleView.setVisibility(View.GONE);
                ((TileActivity) Battle.this.activity).hideBattle();
                if (battleMenuViewGroup != null) {
                    battleMenuViewGroup.removeAllViews();
                }
            }
        });
    }

    public void battleInfoViewFromMonster(final Monster monster, final ViewGroup root) {
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View battleMonsterInfoView = Battle.this.activity.getLayoutInflater().inflate(
                        R.layout.layout_battle_monster_info, root);
                TextView battleMonsterInfo = (TextView) battleMonsterInfoView.findViewById(R.id.battle_monster_info);
                battleMonsterInfo.setText(String.format(
                        Locale.getDefault(),
                        Battle.this.activity.getString(R.string.battle_monster_info),
                        monster.getName(),
                        monster.getGender(),
                        monster.getLevel(),
                        monster.getHitPoints(),
                        monster.getHitPoints()));
                // ViewGroup parentView = (ViewGroup) battleMonsterInfo.getParent();
                // parentView.removeView(battleMonsterInfoView);
            }
        });
    }

    public void showBattleInfo(final String info, final ViewGroup root) {
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final View battleInfoView = Battle.this.activity.getLayoutInflater().inflate(
                        R.layout.layout_battle_info, root);
                TextView battleInfo = (TextView) battleInfoView.findViewById(R.id.battle_info_lbl);
                battleInfo.setText(info);
                Button battleButton = (Button) battleInfoView.findViewById(R.id.battle_info_btn);
                battleButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        root.removeAllViews();
                        showBattleMenu(root);
                        // battleInfoView.setVisibility(View.GONE);
                    }
                });
                Battle.this.battleMenuViewGroup = root;
            }
        });
    }

    public void showBattleMenu(final ViewGroup root) {
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View battleMenuView = Battle.this.activity.getLayoutInflater().inflate(
                        R.layout.layout_battle_menu, root);
                Button runButton = (Button) battleMenuView.findViewById(R.id.battle_menu_btn_run);
                runButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        exitBattle();
                        Debug.e("RUN");
                    }
                });
                Battle.this.battleMenuViewGroup = root;
            }
        });
    }

    /*
    public void monsterInfo() {
        if (!isShowingInfo) {
            String monsterInfo = this.activity.getString(R.string.monster_info,
                    monster.getNature(),
                    monster.getName(),
                    monster.getLevel(),
                    monster.getGender(),
                    monster.getHitPoints(),
                    monster.getAttack(),
                    monster.getDefense(),
                    monster.getSpecialAttack(),
                    monster.getSpecialDefense(),
                    monster.getSpeed());
            label2.setText(monsterInfo);
            isShowingInfo = true;
        } else {
            label2.setText("");
            isShowingInfo = false;
        }
    }
    */
}
