package com.rpgandroid.logic;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 26/03/16.
 */
public class StateMachine {
    private Map<String, IState> mStates;
    private IState mCurrentState;

    public StateMachine() {
        this.mStates = new HashMap<>();
        this.mCurrentState = new EmptyState();
    }

    public void Update(float elapsedTime, Object... params) throws ExecutionException, InterruptedException {
        this.mCurrentState.Update(elapsedTime, params);
    }

    public void Render() { mCurrentState.Render(); }

    public void Change(String stateName, Object... params) {
        mCurrentState.OnExit();
        mCurrentState = mStates.get(stateName);
        mCurrentState.OnEnter(params);
    }

    void Add(String name, IState state) { mStates.put(name, state); }
}
