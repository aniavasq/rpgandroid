package com.rpgandroid.logic;

import java.lang.reflect.Method;
import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 26/03/16.
 */
public interface IState {

    void Update(float elapsedTime, Object... params) throws ExecutionException, InterruptedException;

    void Render();
    void OnEnter(Object... params);
    void OnExit();
}
