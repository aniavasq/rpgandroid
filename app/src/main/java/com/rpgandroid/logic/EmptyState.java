package com.rpgandroid.logic;

import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 26/03/16.
 */
public class EmptyState implements IState {

    @Override
    public void Update(float elapsedTime, Object... params) throws ExecutionException, InterruptedException { }

    @Override
    public void Render() { }

    @Override
    public void OnEnter(Object... params) { }

    @Override
    public void OnExit() { }
}
