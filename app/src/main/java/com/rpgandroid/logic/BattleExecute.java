package com.rpgandroid.logic;

import android.os.AsyncTask;

import com.rpgandroid.logic.action.Action;

import java.util.Stack;
import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 26/03/16.
 */
public class BattleExecute implements IState {
    StateMachine mStateMachine;
    Stack<Action> mActions;

    public BattleExecute(StateMachine mStateMachine, Stack<Action> mActions) {
        this.mStateMachine = mStateMachine;
        this.mActions = mActions;
    }

    @Override
    public void Update(float elapsedTime, Object... params) throws ExecutionException, InterruptedException {
        if (params.length >= 6) {
            int target = (int) params[0];
            int move = (int) params[1];
            Entity entityA = (Entity) params[2];
            Entity entityB = (Entity) params[3];
            Entity entityC = (Entity) params[4];
            Entity entityD = (Entity) params[5];

            AsyncTask performMove = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    return null;
                }
            };
        }
    }

    @Override
    public void Render() { }

    @Override
    public void OnEnter(Object... params) { }

    @Override
    public void OnExit() { }
}
