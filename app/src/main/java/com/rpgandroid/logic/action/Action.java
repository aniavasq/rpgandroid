package com.rpgandroid.logic.action;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 26/03/16.
 */
public class Action<Params, Progress, Result> implements IAction, Comparable<Action> {
    public static Comparator ActionComparator = new Comparator<Action>() {
        @Override
        public int compare(Action lhs, Action rhs) {
            return lhs.compareTo(rhs);
        }
    };

    private float timeRemaining;
    private AsyncTask<Params, Progress, Result> task;
    private boolean ready;

    public Action() {
    }

    public Action(float timeRemaining, AsyncTask<Params, Progress, Result> task) {
        this.timeRemaining = timeRemaining;
        this.task = task;
    }

    @Override
    public void update(float elapsedTime) throws ExecutionException, InterruptedException {
        this.task.execute();
    }

    public void update(float elapsedTime, Params... params) throws ExecutionException, InterruptedException {
        this.task.execute(params);
    }

    @Override
    public boolean isReady() { return this.ready; }

    public float getTimeRemaining(){ return this.timeRemaining; }

    @Override
    public int compareTo(@NonNull Action another) {
        return (int) (this.getTimeRemaining() - another.getTimeRemaining());
    }
}
