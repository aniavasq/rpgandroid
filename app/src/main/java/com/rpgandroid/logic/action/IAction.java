package com.rpgandroid.logic.action;

import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 26/03/16.
 */
public interface IAction<Params, Progress, Result> {
    void update(float elapsedTime) throws ExecutionException, InterruptedException;

    boolean isReady();
}
