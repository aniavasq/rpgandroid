package com.rpgandroid.logic;

/**
 * @author anibal
 * @since 26/03/16.
 */
public class Entity<T> {

    private boolean playerControlled;
    private int speed;
    private T entityObject;

    public Entity() {
        this.playerControlled = true;
        this.speed = 0;
    }

    public Entity(boolean playerControlled, T entityObject) {
        this.playerControlled = playerControlled;
        this.speed = 0;
        this.entityObject = entityObject;
    }

    public Entity(boolean playerControlled, int speed, T entityObject) {
        this.playerControlled = playerControlled;
        this.speed = speed;
        this.entityObject = entityObject;
    }

    public boolean isPlayerControlled() { return playerControlled; }

    public int getSpeed() { return speed; }

    public T getEntityObject() { return entityObject; }

    public void setEntityObject(T entityObject) {
        this.entityObject = entityObject;
    }
}
