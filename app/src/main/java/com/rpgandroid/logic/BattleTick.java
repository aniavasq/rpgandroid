package com.rpgandroid.logic;

import com.rpgandroid.logic.action.Action;

import java.util.Stack;
import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 10/10/16.
 */

public class BattleTick implements IState {
    StateMachine mStateMachine;
    Stack<Action> mActions;

    public BattleTick(StateMachine mStateMachine, Stack<Action> mActions) {
        this.mStateMachine = mStateMachine;
        this.mActions = mActions;
    }

    @Override
    public void Update(float elapsedTime, Object... params) throws ExecutionException, InterruptedException {
        for (Action a: mActions) {
            a.update(elapsedTime, params);
        }

        if(mActions.peek().isReady()) {
            Action top = mActions.pop();
            mStateMachine.Change("execute", top);
        }
    }

    // Things may happen in these functions but nothing we're interested in.
    @Override
    public void Render() {

    }

    @Override
    public void OnEnter(Object... params) {

    }

    @Override
    public void OnExit() {

    }
}
