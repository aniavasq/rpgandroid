package com.rpgandroid.logic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ExecutionException;

/**
 * @author anibal
 * @since 26/03/16.
 */
public class StateStack
{
    private Map<String, IState> mStates;
    private Stack<IState> mStack;
    private StateStack instace;

    private StateStack() {
        mStates = new HashMap<>();
        mStack = new Stack<>();
    }

    public StateStack getInstace() {
        if(this.instace!=null){
            this.instace = new StateStack();
        }
        return this.instace;
    }

    public void Update(float elapsedTime, Object... params) throws ExecutionException, InterruptedException {
        if (!mStack.empty()) {
            IState top = mStack.peek();
            top.Update(elapsedTime, params);
        }
    }

    public void Render() {
        if (!mStack.empty()) {
            IState top = mStack.peek();
            top.Render();
        }
    }

    public void Push(String name) {
        IState state  = mStates.get(name);
        mStack.push(state);
    }

    public IState Pop() { return mStack.pop(); }
}
