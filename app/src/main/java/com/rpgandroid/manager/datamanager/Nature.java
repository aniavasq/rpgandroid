package com.rpgandroid.manager.datamanager;

/**
 * @author anibal
 * @since 23/10/16.
 */

public enum Nature {

    /**
     * The following table lists each one of the 25 Natures and their effects on a Pokémon.
     *
     * +---------+----------------+----------------+-----------------+-----------------+
     * | Nature  | Increased stat | Decreased stat | Favorite flavor | Disliked flavor |
     * +---------+----------------+----------------+-----------------+-----------------+
     * | Hardy   | —              | —              | —               | —               |
     * | Lonely  | Attack         | Defense        | Spicy           | Sour            |
     * | Brave   | Attack         | Speed          | Spicy           | Sweet           |
     * | Adamant | Attack         | Sp. Attack     | Spicy           | Dry             |
     * | Naughty | Attack         | Sp. Defense    | Spicy           | Bitter          |
     * | Bold    | Defense        | Attack         | Sour            | Spicy           |
     * | Docile  | —              | —              | —               | —               |
     * | Relaxed | Defense        | Speed          | Sour            | Sweet           |
     * | Impish  | Defense        | Sp. Attack     | Sour            | Dry             |
     * | Lax     | Defense        | Sp. Defense    | Sour            | Bitter          |
     * | Timid   | Speed          | Attack         | Sweet           | Spicy           |
     * | Hasty   | Speed          | Defense        | Sweet           | Sour            |
     * | Serious | —              | —              | —               | —               |
     * | Jolly   | Speed          | Sp. Attack     | Sweet           | Dry             |
     * | Naive   | Speed          | Sp. Defense    | Sweet           | Bitter          |
     * | Modest  | Sp. Attack     | Attack         | Dry             | Spicy           |
     * | Mild    | Sp. Attack     | Defense        | Dry             | Sour            |
     * | Quiet   | Sp. Attack     | Speed          | Dry             | Sweet           |
     * | Bashful | —              | —              | —               | —               |
     * | Rash    | Sp. Attack     | Sp. Defense    | Dry             | Bitter          |
     * | Calm    | Sp. Defense    | Attack         | Bitter          | Spicy           |
     * | Gentle  | Sp. Defense    | Defense        | Bitter          | Sour            |
     * | Sassy   | Sp. Defense    | Speed          | Bitter          | Sweet           |
     * | Careful | Sp. Defense    | Sp. Attack     | Bitter          | Dry             |
     * | Quirky  | —              | —              | —               | —               |
     * +---------+----------------+----------------+-----------------+-----------------+
     * **/

    HARDY,
    LONELY,
    BRAVE,
    ADAMANT,
    NAUGHTY,
    BOLD,
    DOCILE,
    RELAXED,
    IMPISH,
    LAX,
    TIMID,
    HASTY,
    SERIOUS,
    JOLLY,
    NAIVE,
    MODEST,
    MILD,
    QUIET,
    BASHFUL,
    RASH,
    CALM,
    GENTLE,
    SASSY,
    CAREFUL,
    QUIRKY;

    public int[] getNatureValues() {
        int[] natureValues = new int[]{100, 100, 100, 100, 100, 100};
        if (this == LONELY ||
            this == BRAVE ||
            this == ADAMANT ||
            this == NAUGHTY) {
            natureValues[1] = 110;
        }
        if (this == BOLD ||
                this == TIMID ||
                this == MODEST ||
                this == CALM) {
            natureValues[1] = 90;
        }
        if (this == BOLD ||
                this == RELAXED ||
                this == IMPISH ||
                this == LAX) {
            natureValues[2] = 110;
        }
        if (this == LONELY ||
                this == HASTY ||
                this == MILD ||
                this == GENTLE) {
            natureValues[2] = 90;
        }
        if (this == MODEST ||
                this == MILD ||
                this == QUIET ||
                this == RASH) {
            natureValues[3] = 110;
        }
        if (this == ADAMANT ||
                this == IMPISH ||
                this == JOLLY ||
                this == CAREFUL) {
            natureValues[3] = 90;
        }
        if (this == CALM ||
                this == GENTLE ||
                this == SASSY ||
                this == CAREFUL) {
            natureValues[4] = 110;
        }
        if (this == NAUGHTY ||
                this == LAX ||
                this == NAIVE ||
                this == RASH) {
            natureValues[4] = 90;
        }
        if (this == TIMID ||
                this == HASTY ||
                this == JOLLY ||
                this == NAIVE) {
            natureValues[5] = 110;
        }
        if (this == BRAVE ||
                this == RELAXED ||
                this == QUIET ||
                this == SASSY) {
            natureValues[5] = 90;
        }
        return natureValues;
    }
}
