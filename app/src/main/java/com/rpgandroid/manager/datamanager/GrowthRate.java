package com.rpgandroid.manager.datamanager;

/**
 * @author anibal
 * @since 31/10/16.
 */

public enum GrowthRate {
    SLOW,
    MEDIUM,
    FAST,
    MEDIUM_SLOW,
    SLOW_THEN_VERY_FAST,
    FAST_THEN_VERY_SLOW
}
