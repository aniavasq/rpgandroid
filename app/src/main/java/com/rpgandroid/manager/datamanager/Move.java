package com.rpgandroid.manager.datamanager;

import com.rpgandroid.logic.Entity;

/**
 * @author anibal
 * @since 17/10/16.
 */

public class Move {
    // Descriptive attributes
    private int id;
    private String name;
    private String description;

    // In-Battle attributes
    private Type type;
    private Category category;
    private int power;
    private int accuracy;
    private int powerPoints;
    private String tm_hm;
    private float probability;
    private int[] stageModifiers = new int[8];
    private float damage;
    private TargetType targetType;

    // Contest attributes
    private ContestCategory contestCategory;
    private int appeal;
    private int jam;
    private String contestDescription;

    public enum Category {
        PHYSICAL, SPECIAL, STATUS
    }

    public enum ContestCategory {
        BEAUTIFUL, CLEVER, COOL, CUTE, TOUGH
    }

    public enum TargetType {
        SINGLE_TARGET,
        ALL_ADJACENT,
        ALL_ADJACENT_FOES,
        ALL,
        ALL_FOES,
        ALL_ALLIES
    }

    /**
     * +--------------+------------------------+---------------+
     * |    Stage     | Chance of critical hit |               |
     * +--------------+------------------------+---------------+
     * | Gen II-V     | Gen VI                 |               |
     * | +0           | 1/16 (6.25%)           | 1/16 (6.25%)  |
     * | +1           | 1/8 (12.5%)            | 1/8 (12.5%)   |
     * | +2           | 1/4 (25%)              | 1/2 (50%)     |
     * | +3           | 1/3 (33.3%)            | Always (100%) |
     * | +4 and above |                        |               |
     * +--------------+------------------------+---------------+
     * **/

    public Entity perform(Entity<Monster> performer, Entity<Monster> target, float modifier){
        Monster performerMonster = performer.getEntityObject();
        Monster targetMonster = target.getEntityObject();
        float stab = 1, effectiveness = 1;
        Type monsterType_i;
        for(int i=0; i<performerMonster.getTypes().length; i++){
            monsterType_i = performerMonster.getTypes()[i];
            if(monsterType_i == this.type){
                stab = 1.5f;
            }
        }
        for(int i=0; i<targetMonster.getTypes().length; i++){
            monsterType_i = performerMonster.getTypes()[i];
            effectiveness = effectiveness * Type.typeEffectiveness(this, monsterType_i);
        }
        float critical = 1;
        if(this.category == Category.PHYSICAL || this.category == Category.SPECIAL){
            int attack, defense;
            if (this.category == Category.PHYSICAL){
                attack = targetMonster.getAttack();
                defense = targetMonster.getDefense();
            }else {
                attack = targetMonster.getSpecialAttack();
                defense = targetMonster.getSpecialDefense();
            }
            damage = ( ( ( 2 * performerMonster.getLevel() + 10 ) / 250 ) *
                    (attack / defense) * this.power + 2 ) * stab * critical * modifier;
        }
        targetMonster.setHitPoints((int) (targetMonster.getHitPoints() - damage));
        target.setEntityObject(targetMonster);
        return target;
    }

    public Type getType() { return type; }
}
