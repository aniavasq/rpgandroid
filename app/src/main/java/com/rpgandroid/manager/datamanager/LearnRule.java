package com.rpgandroid.manager.datamanager;

import org.easyrules.core.BasicRule;

/**
 * @author anibal
 * @since 31/10/16.
 */

public class LearnRule extends BasicRule{

    public enum LearnMethod{
        LEVEL_UP,
        EGG,
        TUTOR,
        MACHINE,
        STADIUM_SURFING_PIKACHU,
        LIGHT_BALL_EGG,
        COLOSSEUM_PURIFICATION,
        XD_SHADOW,
        XD_PURIFICATION,
        FORM_CHANGE
    }

    @Override
    public boolean evaluate() {
        return super.evaluate();
    }

    @Override
    public void execute() throws Exception {
        super.execute();
    }
}
