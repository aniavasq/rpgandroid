package com.rpgandroid.manager.datamanager;

/**
 * @author anibal
 * @since 08/10/16.
 */
public class Zone {
    String id;

    public Zone(String id) {
        this.id = id;
    }

    public String getId() { return this.id; }
}
