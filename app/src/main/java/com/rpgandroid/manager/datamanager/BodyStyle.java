package com.rpgandroid.manager.datamanager;

/**
 * @author anibal
 * @since 23/10/16.
 */

public enum BodyStyle {
    HEAD_ONLY,
    SERPENTINE,
    FINS,
    HEAD_W_ARMS,
    HEAD_W_BASE,
    TAIL_BIPED,
    HEAD_W_LEGS,
    QUADRUPED,
    SINGLE_SET_WINGS,
    MULTIPED,
    MULTIPLE_BODIES,
    BIPED,
    MULTIPLE_SET_WINGS,
    INSECTOID
}
