package com.rpgandroid.manager.datamanager;

/**
 * @author anibal
 * @since 23/10/16.
 */

public enum EggGroup {
    /**
     * This is a list of Egg Groups by their index number in the games:
     *
     * 1.  Monster Group: Pokémon in this group are saurian/kaiju-like in appearance and nature.
     * 2.  Water 1 Group: Pokémon in this group are amphibious in nature.
     * 3.  Bug Group: Pokémon in this group are insectoid (bug-like) in appearance.
     * 4.  Flying Group: Pokémon in this group are avian (bird-like) in appearance.
     * 5.  Field Group: The largest group, Pokémon here are terrestrial in nature. In Stadium 2,
     *     this Egg Group was known as "Ground".
     * 6.  Fairy Group: Pokémon in this group are petite and considered very cute.
     * 7.  Grass Group: Pokémon in this group are plant-like in appearance. In Stadium 2, this Egg
     *     Group was known as "Plant".
     * 8.  Human-Like Group: Pokémon in this group are fully bipedal. In Stadium 2, this Egg Group
     *     was known as "Humanshape".
     * 9.  Water 3 Group: Pokémon in this group resemble aquatic invertebrates.
     * 10. Mineral Group: Pokémon in this group are inorganic in nature.
     * 11. Amorphous Group: Pokémon in this group are amorphous, having no definite form. In
     *     Stadium 2, this Egg Group was known as "Indeterminate".
     * 12. Water 2 Group: Pokémon in this group are piscine (fish-like) in appearance.
     * 13. Ditto Group: As the name implies, Ditto is the only Pokémon in this group, and is
     *     capable of breeding with all others (regardless of gender) aside from those in the
     *     Undiscovered and the Ditto group.
     * 14. Dragon Group: Pokémon in this group are draconic in appearance.
     * 15. Undiscovered Group: Pokémon in this group are unable to breed. In Stadium 2, this Egg
     *     Group was known as "No eggs".
     * **/


    MONSTER,
    WATER_1,
    BUG,
    FLYING,
    FIELD,
    FAIRY,
    GRASS,
    HUMAN_LIKE,
    WATER_3,
    MINERAL,
    AMORPHOUS,
    WATER_2,
    DITTO,
    DRAGON,
    UNDISCOVERED
}
