package com.rpgandroid.manager.datamanager;

/**
 * @author anibal
 * @since 06/11/16.
 */

public enum EncounterMethod {
    NULL,
    WALK,
    OLD_ROD,
    GOO_ROD,
    SUPER_ROD,
    SURF,
    ROCK_SMASH,
    HEADBUTT,
    DARK_GRASS,
    GRASS_SPOTS,
    CAVE_SPOTS,
    BRIDGE_SPOTS,
    SUPER_ROD_SPOTS,
    SURF_SPOTS;

    public String getEncounterMethodId(){
        switch (this) {
            case NULL:
                return "0";
            case WALK:
                return "1";
            case OLD_ROD:
                return "2";
            case GOO_ROD:
                return "3";
            case SUPER_ROD:
                return "4";
            case SURF:
                return "5";
            case ROCK_SMASH:
                return "6";
            case HEADBUTT:
                return "7";
            case DARK_GRASS:
                return "8";
            case GRASS_SPOTS:
                return "9";
            case CAVE_SPOTS:
                return "10";
            case BRIDGE_SPOTS:
                return "11";
            case SUPER_ROD_SPOTS:
                return "12";
            case SURF_SPOTS:
                return "13";
            default:
                return "-1";
        }
    }
}
