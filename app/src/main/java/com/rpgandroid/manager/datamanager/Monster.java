package com.rpgandroid.manager.datamanager;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;

import com.rpgandroid.rpgandroid.DataBaseHelper;
import com.rpgandroid.rpgandroid.Utils;

import org.andengine.util.debug.Debug;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

/**
 * @author anibal
 * @since 03/11/15.
 */
public class Monster implements Serializable {
    // Static
    private String name;
    private int number;
    private String id;
    private ArrayList<Monster> forms;
    private boolean hasGenderDifferences;
    private int[] baseStats = new int[6];
    private int baseFriendship;
    private float catchRate;
    private float hatchTime;
    private GenderRatio genderRatio;
    private GrowthRate growthRate;
    private String specie;

    // Immutable
    private float height;
    private float weight;
    private Gender gender;
    private Type[] types = new Type[2]; // can more than 2
    private Nature nature;
    private EggGroup[] eggGroups = new EggGroup[2];
    private BodyStyle bodyStyle;
    private Color color;
    private int[] individualValues = new int[6];
    private  boolean shiny;

    // Mutable Out-Battle
    private String nickname;
    private int hitPoints;
    private int attack;
    private int defense;
    private int specialAttack;
    private int specialDefense;
    private int speed;
    private int level;
    private int experience;
    private int[] effortValues = new int[6];
    private Move[] moves = new Move[4];
    private ArrayList<LearnRule> learnSet;
    private Item[] heldItems = new Item[1];

    // Mutable In-Battle
    private int evasionStage;
    private int accuracyStage;
    private int attackStage;
    private int defenseStage;
    private int specialAttackStage;
    private int specialDefenseStage;
    private int speedStage;
    private boolean fainted;

    // Conditional mutable
    private Ability ability;
    private int form;

    public Monster(String name,
                   int number,
                   String id,
                   ArrayList<Monster> forms,
                   boolean hasGenderDifferences,
                   int[] baseStats,
                   int baseFriendship,
                   float catchRate,
                   float hatchTime,
                   GenderRatio genderRatio,
                   GrowthRate growthRate,
                   String specie,
                   float height,
                   float weight,
                   Gender gender,
                   Type[] types,
                   Nature nature,
                   EggGroup[] eggGroups,
                   BodyStyle bodyStyle,
                   Color color,
                   int[] individualValues,
                   boolean shiny,
                   String nickname,
                   int hitPoints,
                   int attack,
                   int defense,
                   int specialAttack,
                   int specialDefense,
                   int speed, int level,
                   int experience,
                   int[] effortValues,
                   Move[] moves,
                   ArrayList<LearnRule> learnSet,
                   Item[] heldItems,
                   int evasionStage,
                   int accuracyStage,
                   int attackStage,
                   int defenseStage,
                   int specialAttackStage,
                   int specialDefenseStage,
                   int speedStage,
                   boolean fainted,
                   Ability ability,
                   int form) {
        this.name = name;
        this.number = number;
        this.id = id;
        this.forms = forms;
        this.hasGenderDifferences = hasGenderDifferences;
        this.baseStats = baseStats;
        this.baseFriendship = baseFriendship;
        this.catchRate = catchRate;
        this.hatchTime = hatchTime;
        this.genderRatio = genderRatio;
        this.growthRate = growthRate;
        this.specie = specie;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
        this.types = types;
        this.nature = nature;
        this.eggGroups = eggGroups;
        this.bodyStyle = bodyStyle;
        this.color = color;
        this.individualValues = individualValues;
        this.shiny = shiny;
        this.nickname = nickname;
        this.hitPoints = hitPoints;
        this.attack = attack;
        this.defense = defense;
        this.specialAttack = specialAttack;
        this.specialDefense = specialDefense;
        this.speed = speed;
        this.level = level;
        this.experience = experience;
        this.effortValues = effortValues;
        this.moves = moves;
        this.learnSet = learnSet;
        this.heldItems = heldItems;
        this.evasionStage = evasionStage;
        this.accuracyStage = accuracyStage;
        this.attackStage = attackStage;
        this.defenseStage = defenseStage;
        this.specialAttackStage = specialAttackStage;
        this.specialDefenseStage = specialDefenseStage;
        this.speedStage = speedStage;
        this.fainted = fainted;
        this.ability = ability;
        this.form = form;
    }

    /**
     * When a move is used that increases or decreases a stat of a Pokémon in battle, it will be multiplied according to the following fractions, depending on the generation:
     *
     * For Attack, Defense, Sp. Attack, Sp. Defense, and Speed
     * Stage multipliers
     * +----------+--------+--------+--------+--------+--------+--------+---------+---------+---------+---------+---------+---------+---------+
     * |  Stage   |   -6   |   -5   |   -4   |   -3   |   -2   |   -1   |    0    |    1    |    2    |    3    |    4    |    5    |    6    |
     * +----------+--------+--------+--------+--------+--------+--------+---------+---------+---------+---------+---------+---------+---------+
     * | Gen I-II | 25/100 | 28/100 | 33/100 | 40/100 | 50/100 | 66/100 | 100/100 | 150/100 | 200/100 | 250/100 | 300/100 | 350/100 | 400/100 |
     * | Gen III+ | 2/8    | 2/7    | 2/6    | 2/5    | 2/4    | 2/3    | 2/2     | 3/2     | 4/2     | 5/2     | 6/2     | 7/2     | 8/2     |
     * +----------+--------+--------+--------+--------+--------+--------+---------+---------+---------+---------+---------+---------+---------+
     *
     * For accuracy and evasion
     * Stage multipliers
     * +------------------+--------+--------+--------+--------+--------+--------+---------+---------+---------+---------+---------+---------+---------+
     * | Stage (accuracy) |   -6   |   -5   |   -4   |   -3   |   -2   |   -1   |    0    |   +1    |   +2    |   +3    |   +4    |   +5    |   +6    |
     * +------------------+--------+--------+--------+--------+--------+--------+---------+---------+---------+---------+---------+---------+---------+
     * | Stage (evasion)  | +6     | +5     | +4     | +3     | +2     | +1     | 0       | -1      | -2      | -3      | -4      | -5      | -6      |
     * | Gen I            | 25/100 | 28/100 | 33/100 | 40/100 | 50/100 | 66/100 | 100/100 | 150/100 | 200/100 | 250/100 | 300/100 | 350/100 | 400/100 |
     * | Gen II           | 33/100 | 36/100 | 43/100 | 50/100 | 60/100 | 75/100 | 100/100 | 133/100 | 166/100 | 200/100 | 233/100 | 266/100 | 300/100 |
     * | Gen III-IV       | 33/100 | 36/100 | 43/100 | 50/100 | 60/100 | 75/100 | 100/100 | 133/100 | 166/100 | 200/100 | 250/100 | 266/100 | 300/100 |
     * | Gen V+           | 3/9    | 3/8    | 3/7    | 3/6    | 3/5    | 3/4    | 3/3     | 4/3     | 5/3     | 6/3     | 7/3     | 8/3     | 9/3     |
     * +------------------+--------+--------+--------+--------+--------+--------+---------+---------+---------+---------+---------+---------+---------+
     *
     * **/



    public Monster(int number, String name, String id) {
        this.number = number;
        this.name = name;
        this.id = id;
        forms = new ArrayList<>();
    }

    public Monster(Cursor monster_cursor, Cursor form_cursor){
        setName(monster_cursor.getString(1));
        setNumber(monster_cursor.getInt(0));
        setId(String.format(Locale.getDefault(), "%03d", getNumber()));

        forms = new ArrayList<>();
        int i = 1;
        try {
            while (form_cursor.moveToNext()) {
                Monster form = new Monster(
                        i,
                        form_cursor.getString(1) + " " + form_cursor.getString(2),
                        getId() + "_" + i
                );
                if (form_cursor.getString(2) != null) {
                    forms.add(form);
                }
                i++;
            }
        }catch (Exception ignored){ }
        finally {
            form_cursor.close();
        }
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public int getNumber() { return number; }

    public String getId() { return id; }

    public Gender getGender() { return gender; }

    public void setNumber(int number) { this.number = number; }

    public void setId(String id) { this.id = id; }

    public ArrayList<Monster> getForms() { return forms; }

    @Override
    public String toString() { return this.getId() + " " + this.getName(); }

    public Type[] getTypes() {
        return types;
    }

    public int getLevel(){ return this.level; }

    public int getHitPoints() { return hitPoints; }

    public void setHitPoints(int hitPoints) {
        if (hitPoints >= 0) {
            this.hitPoints = hitPoints;
        } else {
            this.hitPoints = 0;
        }
    }

    public int getAttack() { return attack; }

    public void setAttack(int attack) { this.attack = attack; }

    public int getDefense() { return defense; }

    public void setDefense(int defense) { this.defense = defense; }

    public int getSpecialAttack() { return specialAttack; }

    public void setSpecialAttack(int specialAttack) { this.specialAttack = specialAttack; }

    public int getSpecialDefense() { return specialDefense; }

    public void setSpecialDefense(int specialDefense) { this.specialDefense = specialDefense; }

    public int getSpeed() { return speed; }

    public void setSpeed(int speed) { this.speed = speed; }

    public Move getMove(int move){ return this.moves[move]; }

    public int getForm(){ return this.form; }

    public void setBaseStats(int[] baseStats){ this.baseStats = baseStats; }

    public int[] getBaseStats() { return this.baseStats; }

    public int[] getIndividualValues() { return this.individualValues; }

    public int[] getEffortValues() { return this.effortValues; }

    public Nature getNature() { return nature; }

    public void generateId() {
        // Log.e("NAME", this.getName());
        int formCount = this.getForm() - 1;
        if(formCount <= 0) {
            this.setId(String.format(
                    Locale.getDefault(),
                    "%03d",
                    this.getNumber()
            ));
        } else {
            this.setId(String.format(
                    Locale.getDefault(),
                    "%03d_%d",
                    this.getNumber(),
                    formCount
            ));
        }
    }

    public void generateRandomGender(){
        GenderRatio genderRatio = this.genderRatio;
        if (genderRatio.isGenderless()) {
            this.gender = Gender.GENDERLESS;
            return;
        }
        float maleRatio = genderRatio.getMaleRatio();
        float femaleRatio = genderRatio.getFemaleRatio();
        Debug.e("Female Ratio " + femaleRatio);
        boolean isFemale = Utils.hitFromProbability(femaleRatio);
        Debug.e("IsFemale " + isFemale);
        boolean isMale = Utils.hitFromProbability(maleRatio);
        Debug.e("IsMale " + isMale);
        if ( (isFemale && isMale) || (!isFemale && !isMale)) {
            isFemale = Utils.tossCoin();
            if (isFemale) {
                this.gender = Gender.FEMALE;
            } else {
                this.gender = Gender.MALE;
            }
            return;
        }
        if (isFemale) {
            this.gender = Gender.FEMALE;
        } else {
            this.gender = Gender.MALE;
        }
    }

    public void generateRandomGenes() {
        int[] individualValues = new int[6];
        for(int i=0; i<6; i++) {
            individualValues[i] = Utils.getValueInRange(0, 31);
        }
        this.individualValues = individualValues;
    }

    public void generateRandomNature() {
        this.nature = Nature.values()[ Utils.getValueInRange(0, 24) ];
    }

    public void generateRandomStats() {
        int[] stats = new int[6];
        int[] baseStats = this.getBaseStats();
        int[] individualValues = this.getIndividualValues();
        int[] effortValues = this.getEffortValues();
        int[] natureValues = this.nature.getNatureValues();

        stats[0] = ( ( 2 * baseStats[0] + individualValues[0] + effortValues[0] / 4 )
                * this.getLevel() / 100 ) + this.getLevel() + 10;
        for (int i=1; i<6; i++) {
            stats[i] = ( ( 2 * baseStats[i] + individualValues[i] + effortValues[i] / 4 ) *
                    this.getLevel() / 100 ) + 5 * ( natureValues[i] / 100 );
        }
        this.setHitPoints(stats[0]);
        this.setAttack(stats[1]);
        this.setDefense(stats[2]);
        this.setSpecialAttack(stats[3]);
        this.setSpecialDefense(stats[4]);
        this.setSpeed(stats[5]);
    }

    public void generateRandomAttribute(DataBaseHelper dbh) {
        SQLiteDatabase db = dbh.getWritableDatabase();
        String queryString = "SELECT * " +
                "FROM pokemon_stats a " +
                "WHERE a.pokemon_id=?";
        Cursor baseStatsCursor = db.rawQuery(queryString, new String[]{this.getId()});
        int[] baseStats = new int[6];
        while (baseStatsCursor.moveToNext()) {
            baseStats[baseStatsCursor.getInt(1) - 1] = baseStatsCursor.getInt(2);
        }
        baseStatsCursor.close();
        this.setBaseStats(baseStats);
        this.generateRandomGenes();
        this.generateRandomNature();
        this.generateRandomStats();
        this.generateRandomGender();
    }
}
