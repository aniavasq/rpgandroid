package com.rpgandroid.manager.datamanager;

/**
 * @author anibal
 * @since 23/10/16.
 */

public enum Gender {
    FEMALE,
    MALE,
    GENDERLESS;

    @Override
    public String toString() {
        switch (this) {
            case FEMALE:
                return "♀";
            case MALE:
                return "♂";
            default:
                return "";
        }
    }
}
