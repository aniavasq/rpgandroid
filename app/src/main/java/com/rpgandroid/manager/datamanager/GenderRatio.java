package com.rpgandroid.manager.datamanager;

import java.io.Serializable;

/**
 * @author anibal
 * @since 04/11/16.
 */

public class GenderRatio implements Serializable {
    /**
     * Gender Ratios
     *
     * 0♀ - 0♂  -> -1
     * 0♀ - 1♂  -> 0
     * 1/8♀ - 7/8♂  -> 1
     * 1/4♀ - 3/4♂  -> 2
     * 1/2♀ - 1/2♂  -> 4
     * 3/4♀ - 1/4♂  -> 6
     * 7/8♀ - 1/8♂  -> 7
     * 1♀ - 0♂  -> 8
     **/

    public final int FEMALE0_MALE0 = -1;
    public final int FEMALE0_MALE1 = 0;
    public final int FEMALE1_MALE7 = 1;
    public final int FEMALE1_MALE3 = 2;
    public final int FEMALE1_MALE1 = 4;
    public final int FEMALE3_MALE1 = 6;
    public final int FEMALE7_MALE1 = 7;
    public final int FEMALE1_MALE0 = 8;

    private int value;
    private float femaleRatio;
    private float maleRatio;
    private boolean genderless = false;

    public GenderRatio(int value) {
        this.value = value;
        switch (this.value) {
            case FEMALE0_MALE0:
                this.femaleRatio = 0;
                this.maleRatio = 0;
                this.genderless = true;
                break;
            case FEMALE0_MALE1:
                this.femaleRatio = 0;
                this.maleRatio = 1;
                break;
            case FEMALE1_MALE7:
                this.femaleRatio = 0.125f;
                this.maleRatio = 0.875f;
                break;
            case FEMALE1_MALE3:
                this.femaleRatio = 0.25f;
                this.maleRatio = 0.75f;
                break;
            case FEMALE1_MALE1:
                this.femaleRatio = 0.5f;
                this.maleRatio = 0.5f;
                break;
            case FEMALE3_MALE1:
                this.femaleRatio = 0.75f;
                this.maleRatio = 0.25f;
                break;
            case FEMALE7_MALE1:
                this.femaleRatio = 0.875f;
                this.maleRatio = 0.125f;
                break;
            case FEMALE1_MALE0:
                this.femaleRatio = 1;
                this.maleRatio = 0;
                break;
            default:
                this.femaleRatio = 1;
                this.maleRatio = 1;
        }
    }

    public float getMaleRatio(){ return this.maleRatio; }

    public float getFemaleRatio() { return femaleRatio; }

    public boolean isGenderless() { return genderless; }
}
