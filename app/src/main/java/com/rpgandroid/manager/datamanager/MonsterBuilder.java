package com.rpgandroid.manager.datamanager;

import android.graphics.Color;

import java.util.ArrayList;

/**
 * @author anibal
 * @since 23/10/16.
 */

public class MonsterBuilder {
    // Static
    private String name;
    private int number;
    private String id;
    private ArrayList<Monster> forms;
    private boolean hasGenderDifferences;
    private int[] baseStats = new int[6];
    private int baseFriendship;
    private float catchRate;
    private float hatchTime;
    private GenderRatio genderRatio;
    private GrowthRate growthRate;
    private String specie;

    // Immutable
    private float height;
    private float weight;
    private Gender gender;
    private Type[] types = new Type[2]; // can more than 2
    private Nature nature;
    private EggGroup[] eggGroups = new EggGroup[2];
    private BodyStyle bodyStyle;
    private Color color;
    private int[] individualValues = new int[6];
    private  boolean shiny;

    // Mutable Out-Battle
    private String nickname;
    private int hitPoints;
    private int attack;
    private int defense;
    private int specialAttack;
    private int specialDefense;
    private int speed;
    private int level;
    private int experience;
    private int[] effortValues = new int[6];
    private Move[] moves = new Move[4];
    private ArrayList<LearnRule> learnSet;
    private Item[] heldItems = new Item[1];

    // Mutable In-Battle
    private int evasionStage;
    private int accuracyStage;
    private int attackStage;
    private int defenseStage;
    private int specialAttackStage;
    private int specialDefenseStage;
    private int speedStage;
    private boolean fainted;

    // Conditional mutable
    private Ability ability;
    private int form;

    public MonsterBuilder() { }

    public MonsterBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public MonsterBuilder setNumber(int number) {
        this.number = number;
        return this;
    }

    public MonsterBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public MonsterBuilder setForms(ArrayList<Monster> forms) {
        this.forms = forms;
        return this;
    }

    public MonsterBuilder setHasGenderDifferences(boolean hasGenderDifferences) {
        this.hasGenderDifferences = hasGenderDifferences;
        return this;
    }

    public MonsterBuilder setBaseStats(int[] baseStats) {
        this.baseStats = baseStats;
        return this;
    }

    public MonsterBuilder setBaseFriendship(int baseFriendship) {
        this.baseFriendship = baseFriendship;
        return this;
    }

    public MonsterBuilder setCatchRate(float catchRate) {
        this.catchRate = catchRate;
        return this;
    }

    public MonsterBuilder setHatchTime(float hatchTime) {
        this.hatchTime = hatchTime;
        return this;
    }

    public MonsterBuilder setGenderRatio(GenderRatio genderRatio) {
        this.genderRatio = genderRatio;
        return this;
    }

    public MonsterBuilder setGrowthRate(GrowthRate growthRate) {
        this.growthRate = growthRate;
        return this;
    }

    public MonsterBuilder setSpecie(String specie) {
        this.specie = specie;
        return this;
    }

    public MonsterBuilder setHeight(float height) {
        this.height = height;
        return this;
    }

    public MonsterBuilder setWeight(float weight) {
        this.weight = weight;
        return this;
    }

    public MonsterBuilder setGender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public MonsterBuilder setTypes(Type[] types) {
        this.types = types;
        return this;
    }

    public MonsterBuilder setNature(Nature nature) {
        this.nature = nature;
        return this;
    }

    public MonsterBuilder setEggGroups(EggGroup[] eggGroups) {
        this.eggGroups = eggGroups;
        return this;
    }

    public MonsterBuilder setBodyStyle(BodyStyle bodyStyle) {
        this.bodyStyle = bodyStyle;
        return this;
    }

    public MonsterBuilder setColor(Color color) {
        this.color = color;
        return this;
    }

    public MonsterBuilder setIndividualValues(int[] individualValues) {
        this.individualValues = individualValues;
        return this;
    }

    public MonsterBuilder setShiny(boolean shiny) {
        this.shiny = shiny;
        return this;
    }

    public MonsterBuilder setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public MonsterBuilder setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
        return this;
    }

    public MonsterBuilder setAttack(int attack) {
        this.attack = attack;
        return this;
    }

    public MonsterBuilder setDefense(int defense) {
        this.defense = defense;
        return this;
    }

    public MonsterBuilder setSpecialAttack(int specialAttack) {
        this.specialAttack = specialAttack;
        return this;
    }

    public MonsterBuilder setSpecialDefense(int specialDefense) {
        this.specialDefense = specialDefense;
        return this;
    }

    public MonsterBuilder setSpeed(int speed) {
        this.speed = speed;
        return this;
    }

    public MonsterBuilder setLevel(int level) {
        this.level = level;
        return this;
    }

    public MonsterBuilder setExperience(int experience) {
        this.experience = experience;
        return this;
    }

    public MonsterBuilder setEffortValues(int[] effortValues) {
        this.effortValues = effortValues;
        return this;
    }

    public MonsterBuilder setMoves(Move[] moves) {
        this.moves = moves;
        return this;
    }

    public MonsterBuilder setLearnSet(ArrayList<LearnRule> learnSet) {
        this.learnSet = learnSet;
        return this;
    }

    public MonsterBuilder setHeldItems(Item[] heldItems) {
        this.heldItems = heldItems;
        return this;
    }

    public MonsterBuilder setEvasionStage(int evasionStage) {
        this.evasionStage = evasionStage;
        return this;
    }

    public MonsterBuilder setAccuracyStage(int accuracyStage) {
        this.accuracyStage = accuracyStage;
        return this;
    }

    public MonsterBuilder setAttackStage(int attackStage) {
        this.attackStage = attackStage;
        return this;
    }

    public MonsterBuilder setDefenseStage(int defenseStage) {
        this.defenseStage = defenseStage;
        return this;
    }

    public MonsterBuilder setSpecialAttackStage(int specialAttackStage) {
        this.specialAttackStage = specialAttackStage;
        return this;
    }

    public MonsterBuilder setSpecialDefenseStage(int specialDefenseStage) {
        this.specialDefenseStage = specialDefenseStage;
        return this;
    }

    public MonsterBuilder setSpeedStage(int speedStage) {
        this.speedStage = speedStage;
        return this;
    }

    public MonsterBuilder setFainted(boolean fainted) {
        this.fainted = fainted;
        return this;
    }

    public MonsterBuilder setAbility(Ability ability) {
        this.ability = ability;
        return this;
    }

    public MonsterBuilder setForm(int form) {
        this.form = form;
        return this;
    }

    public Monster createMonster(){
        return new Monster(name,
                number,
                id,
                forms,
                hasGenderDifferences,
                baseStats,
                baseFriendship,
                catchRate,
                hatchTime,
                genderRatio,
                growthRate,
                specie,
                height,
                weight,
                gender,
                types,
                nature,
                eggGroups,
                bodyStyle,
                color,
                individualValues,
                shiny,
                nickname,
                hitPoints,
                attack,
                defense,
                specialAttack,
                specialDefense,
                speed,
                level,
                experience,
                effortValues,
                moves,
                learnSet,
                heldItems,
                evasionStage,
                accuracyStage,
                attackStage,
                defenseStage,
                specialAttackStage,
                specialDefenseStage,
                speedStage,
                fainted,
                ability,
                form);
    }
}
