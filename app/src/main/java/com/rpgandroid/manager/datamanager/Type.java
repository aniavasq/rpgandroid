package com.rpgandroid.manager.datamanager;

/**
 * @author anibal
 * @since 17/10/16.
 */

public enum Type {
    NONE,
    NORMAL,
    FIGHTING,
    FLYING,
    POISON,
    GROUND,
    ROCK,
    BUG,
    GHOST,
    STEEL,
    FIRE,
    WATER,
    GRASS,
    ELECTRIC,
    PSYCHIC,
    ICE,
    DRAGON,
    DARK,
    FAIRY,
    UNDEFINED,
    BIRD;

    /**
     * +------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
     * | "DEFENSE → |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
     * +------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
     * | ATTACK ↴"  | NOR | FIR | WAT | ELE | GRA | ICE | FIG | POI | GRO | FLY | PSY | BUG | ROC | GHO | DRA | DAR | STE | FAI |
     * | NORMAL     |     |     |     |     |     |     |     |     |     |     |     |     | ½   | 0   |     |     | ½   |     |
     * | FIRE       |     | ½   | ½   |     | 2   | 2   |     |     |     |     |     | 2   | ½   |     | ½   |     | 2   |     |
     * | WATER      |     | 2   | ½   |     | ½   |     |     |     | 2   |     |     |     | 2   |     | ½   |     |     |     |
     * | ELECTRIC   |     |     | 2   | ½   | ½   |     |     |     | 0   | 2   |     |     |     |     | ½   |     |     |     |
     * | GRASS      |     | ½   | 2   |     | ½   |     |     | ½   | 2   | ½   |     | ½   | 2   |     | ½   |     | ½   |     |
     * | ICE        |     | ½   | ½   |     | 2   | ½   |     |     | 2   | 2   |     |     |     |     | 2   |     | ½   |     |
     * | FIGHTING   | 2   |     |     |     |     | 2   |     | ½   |     | ½   | ½   | ½   | 2   | 0   |     | 2   | 2   | ½   |
     * | POISON     |     |     |     |     | 2   |     |     | ½   | ½   |     |     |     | ½   | ½   |     |     | 0   | 2   |
     * | GROUND     |     | 2   |     | 2   | ½   |     |     | 2   |     | 0   |     | ½   | 2   |     |     |     | 2   |     |
     * | FLYING     |     |     |     | ½   | 2   |     | 2   |     |     |     |     | 2   | ½   |     |     |     | ½   |     |
     * | PSYCHIC    |     |     |     |     |     |     | 2   | 2   |     |     | ½   |     |     |     |     | 0   | ½   |     |
     * | BUG        |     | ½   |     |     | 2   |     | ½   | ½   |     | ½   | 2   |     |     | ½   |     | 2   | ½   | ½   |
     * | ROCK       |     | 2   |     |     |     | 2   | ½   |     | ½   | 2   |     | 2   |     |     |     |     | ½   |     |
     * | GHOST      | 0   |     |     |     |     |     |     |     |     |     | 2   |     |     | 2   |     | ½   |     |     |
     * | DRAGON     |     |     |     |     |     |     |     |     |     |     |     |     |     |     | 2   |     | ½   | 0   |
     * | DARK       |     |     |     |     |     |     | ½   |     |     |     | 2   |     |     | 2   |     | ½   |     | ½   |
     * | STEEL      |     | ½   | ½   | ½   |     | 2   |     |     |     |     |     |     | 2   |     |     |     | ½   | 2   |
     * | FAIRY      |     | ½   |     |     |     |     | 2   | ½   |     |     |     |     |     |     | 2   | 2   | ½   |     |
     * +------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
     */

    public static float typeEffectiveness(Move move, Type monsterType){
        Type monsterTypeName = monsterType;
        switch (move.getType()){
            case NORMAL:
                if((monsterTypeName == ROCK) || (monsterTypeName == STEEL)){
                    return 0.5f;
                } else if(monsterTypeName == GHOST){
                    return 0;
                }
                break;
            case FIRE:
                if((monsterTypeName == FIRE) ||
                        (monsterTypeName == WATER) ||
                        (monsterTypeName == ROCK) ||
                        (monsterTypeName == DRAGON)){
                    return 0.5f;
                } else if((monsterTypeName == GRASS) ||
                        (monsterTypeName == ICE) ||
                        (monsterTypeName == BUG) ||
                        (monsterTypeName == STEEL)){
                    return 2;
                }
                return 1;
            case WATER:
                if((monsterTypeName == WATER) ||
                        (monsterTypeName == GRASS) ||
                        (monsterTypeName == DRAGON)){
                    return 0.5f;
                } else if((monsterTypeName == FIRE) ||
                        (monsterTypeName == GROUND) ||
                        (monsterTypeName == ROCK)){
                    return 2;
                }
                return 1;
            case ELECTRIC:
                if((monsterTypeName == ELECTRIC) ||
                        (monsterTypeName == GRASS) ||
                        (monsterTypeName == DRAGON)){
                    return 0.5f;
                } else if((monsterTypeName == WATER) ||
                        (monsterTypeName == FLYING)){
                    return 2;
                } else if(monsterTypeName == GROUND){
                    return 0;
                }
                return 1;
            case GRASS:
                if((monsterTypeName == FIRE) ||
                        (monsterTypeName == GRASS) ||
                        (monsterTypeName == POISON) ||
                        (monsterTypeName == FLYING) ||
                        (monsterTypeName == BUG) ||
                        (monsterTypeName == DRAGON) ||
                        (monsterTypeName == STEEL)){
                    return 0.5f;
                } else if((monsterTypeName == WATER) ||
                        (monsterTypeName == GROUND) ||
                        (monsterTypeName == ROCK)){
                    return 2;
                }
                return 1;
            case ICE:
                if((monsterTypeName == FIRE) ||
                        (monsterTypeName == WATER) ||
                        (monsterTypeName == ICE) ||
                        (monsterTypeName == STEEL)){
                    return 0.5f;
                } else if((monsterTypeName == GRASS) ||
                        (monsterTypeName == GROUND) ||
                        (monsterTypeName == FLYING) ||
                        (monsterTypeName == DRAGON)){
                    return 2;
                }
                return 1;
            case FIGHTING:
                if((monsterTypeName == POISON) ||
                        (monsterTypeName == FLYING) ||
                        (monsterTypeName == PSYCHIC) ||
                        (monsterTypeName == BUG) ||
                        (monsterTypeName == FAIRY)){
                    return 0.5f;
                } else if((monsterTypeName == NORMAL) ||
                        (monsterTypeName == ICE) ||
                        (monsterTypeName == ROCK) ||
                        (monsterTypeName == DARK) ||
                        (monsterTypeName == STEEL)){
                    return 2;
                } else if(monsterTypeName == GHOST){
                    return 0;
                }
                return 1;
            case POISON:
                if((monsterTypeName == POISON) ||
                        (monsterTypeName == GROUND) ||
                        (monsterTypeName == ROCK) ||
                        (monsterTypeName == GHOST)){
                    return 0.5f;
                } else if((monsterTypeName == GRASS) ||
                        (monsterTypeName == FAIRY)){
                    return 2;
                } else if(monsterTypeName == STEEL){
                    return 0;
                }
                return 1;
            case GROUND:
                if((monsterTypeName == GRASS) ||
                        (monsterTypeName == BUG)){
                    return 0.5f;
                } else if((monsterTypeName == FIRE) ||
                        (monsterTypeName == ELECTRIC) ||
                        (monsterTypeName == POISON) ||
                        (monsterTypeName == ROCK) ||
                        (monsterTypeName == STEEL)){
                    return 2;
                } else if(monsterTypeName == FLYING){
                    return 0;
                }
                return 1;
            case FLYING:
                if((monsterTypeName == ELECTRIC) ||
                        (monsterTypeName == ROCK) ||
                        (monsterTypeName == STEEL)){
                    return 0.5f;
                } else if((monsterTypeName == GRASS) ||
                        (monsterTypeName == FIGHTING) ||
                        (monsterTypeName == BUG)){
                    return 2;
                }
                return 1;
            case PSYCHIC:
                if((monsterTypeName == PSYCHIC) ||
                        (monsterTypeName == STEEL)){
                    return 0.5f;
                } else if((monsterTypeName == FIGHTING) ||
                        (monsterTypeName == POISON)){
                    return 2;
                } else if(monsterTypeName == DARK){
                    return 0;
                }
                return 1;
            case BUG:
                if((monsterTypeName == FIRE) ||
                        (monsterTypeName == FIGHTING) ||
                        (monsterTypeName == POISON) ||
                        (monsterTypeName == FLYING) ||
                        (monsterTypeName == ROCK) ||
                        (monsterTypeName == STEEL) ||
                        (monsterTypeName == FAIRY)){
                    return 0.5f;
                } else if((monsterTypeName == GRASS) ||
                        (monsterTypeName == PSYCHIC) ||
                        (monsterTypeName == DARK)){
                    return 2;
                }
                return 1;
            case ROCK:
                if((monsterTypeName == FIGHTING) ||
                        (monsterTypeName == GROUND) ||
                        (monsterTypeName == STEEL)){
                    return 0.5f;
                } else if((monsterTypeName == FIRE) ||
                        (monsterTypeName == ICE) ||
                        (monsterTypeName == FLYING) ||
                        (monsterTypeName == BUG)){
                    return 2;
                }
                return 1;
            case GHOST:
                if((monsterTypeName == DARK)){
                    return 0.5f;
                } else if((monsterTypeName == PSYCHIC) ||
                        (monsterTypeName == GHOST)){
                    return 2;
                } else if(monsterTypeName == NORMAL){
                    return 0;
                }
                return 1;
            case DRAGON:
                if((monsterTypeName == STEEL)){
                    return 0.5f;
                } else if((monsterTypeName == DRAGON)){
                    return 2;
                } else if(monsterTypeName == FAIRY){
                    return 0;
                }
                return 1;
            case DARK:
                if((monsterTypeName == FIGHTING) ||
                        (monsterTypeName == DARK) ||
                        (monsterTypeName == FAIRY)){
                    return 0.5f;
                } else if((monsterTypeName == PSYCHIC) ||
                        (monsterTypeName == GHOST)){
                    return 2;
                }
                return 1;
            case STEEL:
                if((monsterTypeName == FIRE) ||
                        (monsterTypeName == WATER) ||
                        (monsterTypeName == ELECTRIC) ||
                        (monsterTypeName == STEEL)){
                    return 0.5f;
                } else if((monsterTypeName == ICE) ||
                        (monsterTypeName == ROCK) ||
                        (monsterTypeName == FAIRY)){
                    return 2;
                }
                return 1;
            case FAIRY:
                if((monsterTypeName == FIRE) ||
                        (monsterTypeName == POISON) ||
                        (monsterTypeName == STEEL)){
                    return 0.5f;
                } else if((monsterTypeName == FIGHTING) ||
                        (monsterTypeName == DRAGON) ||
                        (monsterTypeName == DARK)){
                    return 2;
                }
                return 1;
        }
        return 1;
    }
}
