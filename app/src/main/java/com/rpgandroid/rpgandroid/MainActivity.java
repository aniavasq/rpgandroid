package com.rpgandroid.rpgandroid;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.rpgandroid.manager.datamanager.GenderRatio;
import com.rpgandroid.manager.datamanager.Monster;
import com.rpgandroid.manager.datamanager.MonsterBuilder;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayAdapter<Monster> arrayAdapter;
    private ArrayList<Monster> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // populate the model - a simple a list
        list = new ArrayList<>();

        ////////////////////////////////////////////////////////////////////////////////////////////
        try {
            DataBaseHelper dbh = new DataBaseHelper(this.getBaseContext());
            SQLiteDatabase db = dbh.getWritableDatabase();
            Cursor monsterCursor;
            try {

                String queryString = "SELECT * " +
                        "FROM pokemon a " +
                        "INNER JOIN pokemon_forms b ON a.id=b. pokemon_id " +
                        "LEFT JOIN  pokemon_species c ON a.id = c.id " +
                        "ORDER BY a.id";
                monsterCursor = db.rawQuery(queryString, new String[]{});
                while(monsterCursor.moveToNext()) {
                    MonsterBuilder monsterBuilder = new MonsterBuilder()
                            .setNumber(monsterCursor.getInt(0))
                            .setName(monsterCursor.getString(9))
                            .setForm(monsterCursor.getInt(16))
                            .setLevel(50)
                            .setGenderRatio(new GenderRatio(monsterCursor.getInt(26)))
                            .setEffortValues(new int[]{0, 0, 0, 0, 0, 0});
                    Monster monster = monsterBuilder.createMonster();
                    monster.generateId();
                    list.add(monster);
                }
                monsterCursor.close();
            }catch (Exception ignored){ }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////

        // create our SelectedAdapter
        arrayAdapter = new ArrayAdapter<>(this, R.layout.simplerow, R.id.rowTextView, list);
        arrayAdapter.setNotifyOnChange(true);

        ListView listview = (ListView) findViewById(R.id.monsterList);
        listview.setAdapter(arrayAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //@Override
            public void onItemClick(AdapterView arg0, View view,
                                    int position, long id) {
                // user clicked a list item, make it "selected"

                Intent myIntent = new Intent(MainActivity.this, TileActivity.class);

                // Bundle b = new Bundle();
                // b.putString("monster", list.get(position).getId() ); //Your id
                // myIntent.putExtras(b); //Put your id to your next Intent
                myIntent.putExtra("monster", list.get(position));
                startActivity(myIntent);
                MainActivity.this.startActivity(myIntent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
