package com.rpgandroid.rpgandroid;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.rpgandroid.gui.PrompableText;
import com.rpgandroid.logic.Battle;
import com.rpgandroid.manager.datamanager.EncounterMethod;
import com.rpgandroid.manager.datamanager.Monster;
import com.rpgandroid.manager.datamanager.Zone;

import org.andengine.engine.camera.ZoomCamera;
import org.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.andengine.engine.camera.hud.controls.DigitalOnScreenControl;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.shape.Shape;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLayerProperty;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXLoader.ITMXTilePropertiesListener;
import org.andengine.extension.tmx.TMXObject;
import org.andengine.extension.tmx.TMXObjectGroup;
import org.andengine.extension.tmx.TMXObjectProperty;
import org.andengine.extension.tmx.TMXProperties;
import org.andengine.extension.tmx.TMXTile;
import org.andengine.extension.tmx.TMXTileProperty;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.Constants;
import org.andengine.util.debug.Debug;

import java.io.IOException;
import java.util.Locale;

/**
 * @author anibal.vasquez
 * @since 8/25/2015.
 */

public class TileActivity extends SimpleBaseGameActivity {
    // ===========================================================
    // Constants
    // ===========================================================

    private static int CAMERA_WIDTH = 800;
    private static int CAMERA_HEIGHT = 480;

    private static final float CAMERA_ZOOM_FACTOR = 2f;

    private static final long[] ANIMATE_DURATION = new long[]{75, 75, 75, 75};

    private static final int PLAYER_VELOCITY = 5;

    private static final float ENCOUNTER_PROBABILITY = 0.1f;

    private static final String TILE_MAP = "b_w_outdoor.tmx";

    // ===========================================================
    // Fields
    // ===========================================================

    private ZoomCamera mCamera;

    private ITexture mPlayerTexture;
    private ITexture grassTexture;
    private TiledTextureRegion mPlayerTextureRegion;
    private TextureRegion grassTextureRegion;
    private Body mPlayerBody;

    private ITexture mOnScreenControlBaseTexture;
    private ITextureRegion mOnScreenControlBaseTextureRegion;
    private ITexture mOnScreenControlKnobTexture;
    private ITextureRegion mOnScreenControlKnobTextureRegion;

    private DigitalOnScreenControl mDigitalOnScreenControl;

    private TMXTiledMap mTMXTiledMap;
    private TMXLayer[] waterLayers = new TMXLayer[5];
    //protected int mCactusCount;
    private PlayerDirection playerDirection = PlayerDirection.NONE;

    private Scene mScene;

    private PhysicsWorld mPhysicsWorld;
    private TextView textView;
    private View battleView;

    private boolean isInEncounterZone = false;
    private boolean canMove = true;
    private Battle battle;
    private Zone encounterZone;
    private EncounterMethod encounterMethod;
    private Monster[] monsterFormation = new Monster[6];

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    private IUpdateHandler getGrassUpdateHandler(final AnimatedSprite player,
                                                 final TMXLayer grass_layer,
                                                 final Rectangle currentTileRectangle,
                                                 final Sprite grassSprite) {
        return new IUpdateHandler() {

            @Override
            public void reset() { }

            @Override
            public void onUpdate(final float pSecondsElapsed) {

				/* Get the scene-coordinates of the players feet. */
                final float[] playerFootCoordinates =
                        player.convertLocalCoordinatesToSceneCoordinates(8, 0);

				/* Get the tile the feet of the player are currently waking on. */
                assert grass_layer != null;
                TMXTile tmxTile = grass_layer.getTMXTileAt(playerFootCoordinates[Constants.VERTEX_INDEX_X]+2,
                        playerFootCoordinates[Constants.VERTEX_INDEX_Y]+1);

                if (tmxTile != null) {
                    currentTileRectangle.setPosition(grass_layer.getTileX(tmxTile.getTileColumn()), grass_layer.getTileY(tmxTile.getTileRow()));
                    currentTileRectangle.setZIndex(1);
                }
                grassSprite.setVisible(false);

                try {
                    assert tmxTile != null;
                    TMXProperties<TMXTileProperty> tmxTileProperties = tmxTile.getTMXTileProperties(mTMXTiledMap);
                    if((tmxTileProperties != null) && tmxTileProperties.containsTMXProperty("grass", "true")) {
                        grassSprite.setPosition(grass_layer.getTileX(tmxTile.getTileColumn())-2, grass_layer.getTileY(tmxTile.getTileRow()));
                        grassSprite.setZIndex(1);
                        grassSprite.setVisible(true);
                        mScene.sortChildren();
                    }
                }catch (Exception e){
                    // Debug.e("GRASS",e.getMessage(), e);
                }
            }
        };
    }

    private PhysicsConnector getPhysicsConnector(AnimatedSprite player){
        return new PhysicsConnector(player,
                mPlayerBody, true, false) {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                super.onUpdate(pSecondsElapsed);
                mCamera.updateChaseEntity();
            }
        };
    }

    private TimerHandler getAnimatorTimeHandler() {
        return new TimerHandler(0.6f, true, new ITimerCallback() {
            int t = 0;
            public void onTimePassed(TimerHandler pTimerHandler) {
                if (t < 5) {
                    t++;
                } else {
                    t = 0;
                }
                animateWater(t);
            }
        });
    }

    private ITMXTilePropertiesListener getTilePropertiesListener() {
        return new ITMXTilePropertiesListener() {

            @Override
            public void onTMXTileWithPropertiesCreated(final TMXTiledMap pTMXTiledMap,
                                                       final TMXLayer pTMXLayer,
                                                       final TMXTile pTMXTile,
                                                       final TMXProperties<TMXTileProperty> pTMXTileProperties) {
					/* We are going to count the tiles that have the property "cactus=true" set. */
                /**
                 * TODO: Load NPC (No Playable Characters)
                 * */
            }
        };
    }

    private BaseOnScreenControl.IOnScreenControlListener getOnScreenControlListener(final AnimatedSprite player) {
        return new BaseOnScreenControl.IOnScreenControlListener() {

            @Override
            public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl,
                                        final float pValueX, final float pValueY) {
                if (canMove) {
                    if (pValueY == 1) {
                        // Up
                        if (playerDirection != PlayerDirection.UP) {
                            player.animate(ANIMATE_DURATION, 12, 15, true);
                            playerDirection = PlayerDirection.UP;
                        }
                    } else if (pValueY == -1) {
                        // Down
                        if (playerDirection != PlayerDirection.DOWN) {
                            player.animate(ANIMATE_DURATION, 0, 3, true);
                            playerDirection = PlayerDirection.DOWN;
                        }
                    } else if (pValueX == -1) {
                        // Left
                        if (playerDirection != PlayerDirection.LEFT) {
                            player.animate(ANIMATE_DURATION, 4, 7, true);
                            playerDirection = PlayerDirection.LEFT;
                        }
                    } else if (pValueX == 1) {
                        // Right
                        if (playerDirection != PlayerDirection.RIGHT) {
                            player.animate(ANIMATE_DURATION, 8, 11, true);
                            playerDirection = PlayerDirection.RIGHT;
                        }
                    } else {
                        if (player.isAnimationRunning()) {
                            player.stopAnimation();
                            playerDirection = PlayerDirection.NONE;
                        }
                    }
                    mPlayerBody.setLinearVelocity(pValueX * PLAYER_VELOCITY, pValueY * PLAYER_VELOCITY);
                    if ((pValueY != 0 || pValueX != 0) && isInEncounterZone) {
                    /*
                    * TODO: Create random encounter
                    * */
                        boolean triggerEncounter = isEncounter();
                        Debug.e("Is encounter: " + triggerEncounter);
                        if (triggerEncounter) {
                            showBattle();
                        }
                    }
                } else {
                    mPlayerBody.setLinearVelocity(0, 0);
                }
            }
        };
    }

    public void setEncounterZone(Zone encounterZone) { this.encounterZone = encounterZone; }

    public void setEncounterMethod(EncounterMethod encounterMethod) { this.encounterMethod = encounterMethod; }

    public EncounterMethod getEncounterMethod() { return this.encounterMethod; }

    public Monster getMonsterFromFormation(int i) { return  this.monsterFormation[i]; }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onSetContentView() {
        this.mRenderSurfaceView = new RenderSurfaceView(this);
        this.mRenderSurfaceView.setRenderer(this.mEngine, this);
        final RelativeLayout.LayoutParams surfaceViewLayoutParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT);

        RelativeLayout.LayoutParams textLP =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        textLP.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        textView = new TextView(this);
        textView.setText("Loading...\n\n");
        textView.setBackground(ContextCompat.getDrawable(this, R.drawable.textbox));
        textView.setPadding(20, 20, 20, 20);
        textView.setTextColor(Color.WHITE);
        textView.setLayoutParams(textLP);

        RelativeLayout.LayoutParams battleLP =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT);
        battleView = getLayoutInflater().inflate(R.layout.layout_battle, null);
        Monster monster = (Monster) getIntent().getSerializableExtra("monster");
        battle = new Battle(battleView, this, monster, null, EncounterMethod.WALK);
        battleView.setLayoutParams(battleLP);

        final RelativeLayout frameLayout = new RelativeLayout(this);
        final RelativeLayout.LayoutParams frameLayoutLayoutParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT);

        frameLayout.addView(this.mRenderSurfaceView, surfaceViewLayoutParams);
        frameLayout.addView(textView);
        frameLayout.addView(battleView);
        this.setContentView(frameLayout, frameLayoutLayoutParams);
    }

    @Override
    public EngineOptions onCreateEngineOptions() {

        setCameraDimensions();

        this.mCamera = new ZoomCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        this.mCamera.setZoomFactor(CAMERA_ZOOM_FACTOR);
        this.mCamera.setBoundsEnabled(false);

        return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
                new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mCamera);
    }

    @Override
    public void onCreateResources() throws IOException {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        ////////////////////////////////////////////////////////////////////////////////////////////
        Monster monster = (Monster) getIntent().getSerializableExtra("monster");
        this.monsterFormation[0] = monster;
        this.setEncounterMethod(EncounterMethod.WALK);
        DataBaseHelper dbh = new DataBaseHelper(this.getBaseContext());
        monster.generateRandomAttribute(dbh);
        ////////////////////////////////////////////////////////////////////////////////////////////

        this.mPlayerTexture = new AssetBitmapTexture(this.getTextureManager(),
                this.getAssets(), "gfx/monster/" + monster.getId() + ".png", TextureOptions.DEFAULT);
        this.mPlayerTextureRegion =
                TextureRegionFactory.extractTiledFromTexture(this.mPlayerTexture, 4, 4);
        this.mPlayerTexture.load();

        this.grassTexture = new AssetBitmapTexture(this.getTextureManager(),
                this.getAssets(), "gfx/grass.png", TextureOptions.DEFAULT);
        this.grassTextureRegion = TextureRegionFactory.extractFromTexture(this.grassTexture);
        this.grassTexture.load();

        // Control texture
        this.mOnScreenControlBaseTexture = new AssetBitmapTexture(this.getTextureManager(),
                this.getAssets(), "gfx/onscreen_control_base.png", TextureOptions.BILINEAR);
        this.mOnScreenControlBaseTextureRegion =
                TextureRegionFactory.extractFromTexture(this.mOnScreenControlBaseTexture);
        this.mOnScreenControlBaseTexture.load();

        this.mOnScreenControlKnobTexture = new AssetBitmapTexture(this.getTextureManager(),
                this.getAssets(), "gfx/onscreen_control_knob.png", TextureOptions.BILINEAR);
        this.mOnScreenControlKnobTextureRegion =
                TextureRegionFactory.extractFromTexture(this.mOnScreenControlKnobTexture);
        this.mOnScreenControlKnobTexture.load();

        try {
            final TMXLoader tmxLoader = new TMXLoader(this.getAssets(),
                    this.mEngine.getTextureManager(), TextureOptions.NEAREST_PREMULTIPLYALPHA,
                    this.getVertexBufferObjectManager(), getTilePropertiesListener());
            this.mTMXTiledMap = tmxLoader.loadFromAsset(TILE_MAP);

            this.mTMXTiledMap.setOffsetCenter(0, 0);

            //this.toastOnUiThread("Cactus count in this TMXTiledMap: " + TileActivity.this.mCactusCount, Toast.LENGTH_LONG);
        } catch (final TMXLoadException e) {
            Debug.e(e);
        }
    }

    @Override
    public Scene onCreateScene() {
        this.mEngine.registerUpdateHandler(new FPSLogger());

        this.mScene = new Scene();

        // Create physics world
        this.mPhysicsWorld = new FixedStepPhysicsWorld(30, new Vector2(0, 0), false, 8, 1);
        this.mScene.registerUpdateHandler(this.mPhysicsWorld);

        this.mScene.attachChild(this.mTMXTiledMap);

        TMXLayer grassLayer = null;

        // Add the non-object layers to the scene
        int j = 0;
        for (int i = 0; i < this.mTMXTiledMap.getTMXLayers().size(); i++){
            TMXLayer layer = this.mTMXTiledMap.getTMXLayers().get(i);
            TMXProperties<TMXLayerProperty> layerProperties = layer.getTMXLayerProperties();

            if (i == 0) {
                // Read in the un-walkable blocks from the object layer and create boxes for each
                this.createNoWalkableObjects(layer, 0, 0, 1f);
            } else if (i == 1){
                this.createZoneObjects(layer, 0, 0, 1f);
            }
            if (layerProperties.containsTMXProperty("z_index", "1")) {
                layer.setZIndex(1);
            } else if (layerProperties.containsTMXProperty("water_frame")) {
                waterLayers[j] = layer;
                j ++;
            } else if (layerProperties.containsTMXProperty("grass")) {
                grassLayer = layer;
            }

            layer.detachSelf();
            mScene.attachChild(layer);
        }

		/* Make the camera not exceed the bounds of the TMXEntity. */
        this.mCamera.setBoundsEnabled(false);
        this.mCamera.setBounds(0, 0, this.mTMXTiledMap.getWidth(), this.mTMXTiledMap.getHeight());
        this.mCamera.setBoundsEnabled(true);

        final float centerX = CAMERA_WIDTH / 2;
        final float centerY = CAMERA_HEIGHT / 2;

		/* Create the sprite and add it to the scene. */
        final AnimatedSprite player = new AnimatedSprite(centerX, centerY,
                this.mPlayerTextureRegion, this.getVertexBufferObjectManager());
        player.setOffsetCenterY(0);
        player.setScale(0.5f);
        this.mCamera.setChaseEntity(player);

        final FixtureDef playerFixtureDef = PhysicsFactory.createFixtureDef(0, 0, 0.5f);
        mPlayerBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, player,
                BodyDef.BodyType.DynamicBody, playerFixtureDef);
        this.mPhysicsWorld.registerPhysicsConnector(getPhysicsConnector(player));
        mPlayerBody.setUserData("player");

		/* Now we are going to create a rectangle that will  always highlight the tile below the feet of the pEntity. */
        final Rectangle currentTileRectangle = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(),
                this.mTMXTiledMap.getTileHeight(), this.getVertexBufferObjectManager());
		/* Set the OffsetCenter to 0/0, so that it aligns with the TMXTiles. */
        currentTileRectangle.setOffsetCenter(0, 0);
        currentTileRectangle.setColor(1, 0, 0, 0.25f);
        mScene.attachChild(currentTileRectangle);

        final Sprite grassSprite = new Sprite(0, 0, grassTextureRegion, getVertexBufferObjectManager());
        grassSprite.setOffsetCenter(0, 0);
        mScene.attachChild(grassSprite);

		/* The layer for the player to walk on. */
        final TMXLayer tmxLayer = this.mTMXTiledMap.getTMXLayers().get(0);

        final float width = tmxLayer.getWidth();
        final float height = tmxLayer.getHeight();
        // Add outer walls
        this.addBounds(width, height);

        setPlayerPosition(450f, 200f);

        final TMXLayer grass_layer = grassLayer;

        mScene.registerUpdateHandler(getGrassUpdateHandler(player, grass_layer,
                currentTileRectangle, grassSprite));

        mScene.registerUpdateHandler(getAnimatorTimeHandler());

        mPhysicsWorld.setContactListener(createContactListener());

        mScene.attachChild(player);

        this.mDigitalOnScreenControl = new DigitalOnScreenControl(0, 0, this.mCamera,
                this.mOnScreenControlBaseTextureRegion, this.mOnScreenControlKnobTextureRegion,
                0.1f, this.getVertexBufferObjectManager(),
                getOnScreenControlListener(player));

        final Sprite controlBase = this.mDigitalOnScreenControl.getControlBase();
        controlBase.setAlpha(0.5f);
        controlBase.setOffsetCenter(0, 0);

        this.mDigitalOnScreenControl.getControlKnob().setScale(1.25f);

        mScene.setChildScene(this.mDigitalOnScreenControl);
        mScene.sortChildren();

        hideText();
        hideBattle_();
        return mScene;
    }

    @Override
    public void onBackPressed() {
        Intent myIntent = new Intent(TileActivity.this, MainActivity.class);
        startActivity(myIntent);
        TileActivity.this.startActivity(myIntent);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    private void setCameraDimensions(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int cameraWidth = (int) (metrics.widthPixels/1.5);
        int cameraHeight = (int) (metrics.heightPixels/1.5);

        if (cameraHeight < CAMERA_HEIGHT){
            CAMERA_HEIGHT = (int) (metrics.heightPixels/1.5);
        }

        if (cameraWidth < CAMERA_WIDTH){
            CAMERA_WIDTH = (int) (metrics.widthPixels/1.5);
        }
    }

    private void createNoWalkableObjects(final TMXLayer tmxLayer,
                                         float density,
                                         float elasticity,
                                         float friction){

        float tmxLayer_height = tmxLayer.getHeight();
        VertexBufferObjectManager vertexBufferObjectManager = getVertexBufferObjectManager();


        for(TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("collision", "true")){
                // This is our "wall" layer. Create the boxes from it
                for(TMXObject object : group.getTMXObjects()) {

                    int h = object.getHeight(), w = object.getWidth();

                    Rectangle rect = new Rectangle(w/2 + object.getX(), // + 32,
                            tmxLayer_height - h/2 - object.getY() - 28,
                            w - Math.round(0.11*w),
                            h - Math.round(0.11*h), //- 10,
                            vertexBufferObjectManager);
                    FixtureDef boxFixtureDef = PhysicsFactory.createFixtureDef(density, elasticity, friction);
                    Body boxBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, rect,
                            BodyDef.BodyType.StaticBody, boxFixtureDef);
                    rect.setVisible(false);
                    TMXProperties<TMXObjectProperty> tmxObjectProperties = object.getTMXObjectProperties();
                    if(tmxObjectProperties.containsTMXProperty("prompText")){
                        if (tmxObjectProperties.getTMXProperty("text") != null) {
                            String textValue = tmxObjectProperties.getTMXProperty("text").getValue();
                            Integer labelID = getResources().getIdentifier(textValue, "string", getPackageName());
                            textValue = getString(labelID);
                            PrompableText prompableText = new PrompableText(textValue);
                            boxBody.setUserData(prompableText);
                        }
                    }
                    mScene.attachChild(rect);
                }
            }
        }
    }

    private void createZoneObjects(final TMXLayer tmxLayer,
                                         float density,
                                         float elasticity,
                                         float friction){

        float tmxLayer_height = tmxLayer.getHeight();
        VertexBufferObjectManager vertexBufferObjectManager = getVertexBufferObjectManager();


        for(TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("zone", "true")){
                for(TMXObject object : group.getTMXObjects()) {

                    int h = object.getHeight(), w = object.getWidth();

                    Rectangle rect = new Rectangle(w/2 + object.getX(), // + 32,
                            tmxLayer_height - h/2 - object.getY() - 28,
                            w - Math.round(0.11*w),
                            h - Math.round(0.11*h), //- 10,
                            vertexBufferObjectManager);
                    FixtureDef boxFixtureDef = PhysicsFactory.createFixtureDef(density,
                            elasticity,
                            friction,
                            true);
                    Body boxBody = PhysicsFactory.createBoxBody(this.mPhysicsWorld, rect,
                            BodyDef.BodyType.StaticBody, boxFixtureDef);
                    rect.setVisible(false);
                    TMXProperties<TMXObjectProperty> tmxObjectProperties = object.getTMXObjectProperties();
                    Debug.e("Zone Found");
                    if(tmxObjectProperties.containsTMXProperty("zone_id")){
                        String zoneId = tmxObjectProperties.getTMXProperty("zone_id").getValue();
                        Zone zone = new Zone(zoneId);
                        boxBody.setUserData(zone);
                        Debug.e("Zone Set");
                    }
                    mScene.attachChild(rect);
                }
            }
        }
    }

    private void addBounds(float width, float height){
        float half_width = width / 2, half_height = height / 2;
        VertexBufferObjectManager vertexBufferObjectManager = getVertexBufferObjectManager();

        Shape top = new Rectangle(half_width, height - 18, width + 2, 2, vertexBufferObjectManager);
        top.setVisible(false);
        Shape bottom = new Rectangle(half_width, -32, width - 2, 2, vertexBufferObjectManager);
        bottom.setVisible(false);
        Shape left = new Rectangle(0, half_height, 2, height + 2, vertexBufferObjectManager);
        left.setVisible(false);
        Shape right = new Rectangle(width + 2, half_height, 2, height + 2, vertexBufferObjectManager);
        right.setVisible(false);

        FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0, 0, 0.5f);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, bottom, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, top, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, left, BodyDef.BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, right, BodyDef.BodyType.StaticBody, wallFixtureDef);

        this.mScene.attachChild(bottom);
        this.mScene.attachChild(top);
        this.mScene.attachChild(left);
        this.mScene.attachChild(right);
    }

    private void animateWater(int t) {
        for(int i=waterLayers.length; --i>=0; ) {
            TMXLayer waterLayer = waterLayers[i];
            TMXProperties<TMXLayerProperty> layerProperties = waterLayer.getTMXLayerProperties();
            boolean visible = layerProperties.containsTMXProperty("water_frame", String.valueOf(t+1));
            waterLayer.setVisible(visible);
        }
    }

    private void setPlayerPosition(float positionX, float positionY) {
        positionX = positionX / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;
        positionY = positionY / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;
        mPlayerBody.setTransform(positionX, positionY, 0);
    }

    /*private float[] getPlayerPosition(float positionX, float positionY) {
        Vector2 playerPosition = mPlayerBody.getTransform().getPosition();
        return new float[]{playerPosition.x, playerPosition.y};
    }*/

    private Boolean isEncounter(){
        /**
         * The probability (P) of encountering a Monster after taking a step in an area is
         * determined by a simple mathematical formula:
         * P = x / 187.5
         * where x is one of the values below describing how rare Monster are in the area.
         * +-------------------+-------+
         * | Encounter rarity  |   x   |
         * +-------------------+-------+
         * | Very common       |    10 |
         * | Common            |   8.5 |
         * | Semi-rare         |  6.75 |
         * | Rare              |  3.33 |
         * | Very rare         | 1.25  |
         * +-------------------+-------+
         * http://bulbapedia.bulbagarden.net/wiki/Tall_grass
         **/

        /*int encounterType = 1 + (int)(Math.random() * 4);
        switch (encounterType){
            case 1:
                return Utils.hitFromProbability(10f/187.5f);
            case 2:
                return Utils.hitFromProbability(8.5f/187.5f);
            case 3:
                return Utils.hitFromProbability(6.75f/187.5f);
            case 4:
                return Utils.hitFromProbability(3.33f/187.5f);
            default:
                return Utils.hitFromProbability(1.25f/187.5f);
        }*/
        return Utils.hitFromProbability(ENCOUNTER_PROBABILITY);
    }

    private void showText(final String text){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setVisibility(View.VISIBLE);
                textView.setText(null);
                textView.setText(text);
                mDigitalOnScreenControl.setVisible(false);
            }
        });
    }

    private void hideText(){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setVisibility(View.GONE);
                if (mDigitalOnScreenControl != null) {
                    mDigitalOnScreenControl.setVisible(true);
                }
            }
        });
    }

    private void showTextFromPrompableText(PrompableText prompableText){
        if (prompableText.isScrollable()){
            int j = 1;
            for(int i = 0; i < prompableText.getLinesNumber(); i += 3) {
                String text = prompableText.getLines()[i];
                while (j < i){
                    if(j < prompableText.getLinesNumber()) {
                        text = String.format(Locale.getDefault(),
                                "%s\n%s",
                                text,
                                prompableText.getLines()[j]);
                    } else {
                        text = String.format(Locale.getDefault(),
                                "%s\n",
                                text);
                    }
                    j++;
                }
                showText(text);
            }
        } else {
            String text = prompableText.getLines()[0];
            int j = 1;
            while (j < 3) {
                if(j < prompableText.getLinesNumber()) {
                    text = String.format(Locale.getDefault(), "%s\n%s", text, prompableText.getLines()[j]);
                } else{
                    text = String.format(Locale.getDefault(), "%s\n", text);
                }
                j++;
            }
            showText(text);
        }
    }

    private void showBattle(){
        Monster monster = this.getMonsterFromFormation(0);
        this.battle = new Battle(
                this.battleView,
                this,
                monster,
                this.encounterZone,
                this.getEncounterMethod()
        );
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TileActivity.this.battleView.setVisibility(View.VISIBLE);
                TileActivity.this.battle.showBattle();
                TileActivity.this.mDigitalOnScreenControl.setVisible(false);
                // TileActivity.this.battle.getRandomMonster();
                TileActivity.this.canMove = false;
            }
        });
    }

    public void hideBattle(){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TileActivity.this.battleView.setVisibility(View.GONE);
                TileActivity.this.battle.hideBattle();
                TileActivity.this.mDigitalOnScreenControl.setVisible(true);
                canMove = true;
            }
        });
    }

    private void hideBattle_(){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TileActivity.this.battleView.setVisibility(View.GONE);
            }
        });
    }

    private ContactListener createContactListener()
    {
        return new ContactListener()
        {
            @Override
            public void beginContact(Contact contact)
            {
                Fixture fixtureA = contact.getFixtureA();
                Fixture fixtureB = contact.getFixtureB();
                Object fAData, fBData;
                if ((fAData = fixtureA.getBody().getUserData()) != null &&
                        (fBData = fixtureB.getBody().getUserData()) != null) {
                    if (fAData instanceof PrompableText){
                        PrompableText prompableText = ((PrompableText) fAData);
                        showTextFromPrompableText(prompableText);
                    } else if (fAData instanceof Zone){
                        // Debug.e("Is encounter: "+isEncounter());
                        isInEncounterZone = true;
                        TileActivity.this.setEncounterZone((Zone) fAData);
                    }

                    if(fBData instanceof PrompableText){
                        PrompableText prompableText = ((PrompableText) fBData);
                        showTextFromPrompableText(prompableText);
                    } else if (fBData instanceof Zone){
                        // Debug.e("Is encounter: "+isEncounter());
                        isInEncounterZone = true;
                        TileActivity.this.setEncounterZone((Zone) fBData);
                    }
                }
            }

            @Override
            public void endContact(Contact contact) {
                hideText();

                Fixture fixtureA = contact.getFixtureA();
                Fixture fixtureB = contact.getFixtureB();
                Object fAData, fBData;
                if ((fAData = fixtureA.getBody().getUserData()) != null &&
                        (fBData = fixtureB.getBody().getUserData()) != null) {
                    if (fAData instanceof Zone || fBData instanceof Zone){
                        isInEncounterZone = false;
                    }
                }
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) { }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) { }
        };
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private enum PlayerDirection{
        NONE,
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}
