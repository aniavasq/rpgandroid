package com.rpgandroid.rpgandroid;


import java.util.Random;

/**
 * @author anibal
 * @since 05/10/16.
 */
public class Utils {

    public static Random random = new Random();

    public static Boolean hitFromProbability(Float probability){
        return Math.random() <= probability;
    }

    public static int getValueInRange(int lowerBound, int upperBound){
        return random.nextInt(upperBound - lowerBound) + lowerBound;
    }

    public static Boolean tossCoin() {
        return hitFromProbability(0.5f);
    }
}
